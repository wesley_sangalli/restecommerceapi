package com.api.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.model.Customer;
import com.api.repository.CostumerRepository;

@RestController
public class CostumerController {
	@RequestMapping("/costumer")
	public List<Customer> listClients(){
		return new CostumerRepository().findAll();
	}
}
