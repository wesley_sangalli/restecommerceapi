package com.api.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.api.model.User;
import com.api.repository.UserRepository;

@RestController
public class UserController {
	
	@RequestMapping(value= "/user", method = RequestMethod.POST)
	public String create( @RequestBody User user){
		UserRepository repository = new UserRepository();
		repository.createUser(user);
		return "redirect:/listUsers";
	}
	
	@RequestMapping(value="/users", method = RequestMethod.GET)
	public List<User> listUser(){
		return new UserRepository().findAll();
	}
	
	@RequestMapping(value="/user/{id}", method = RequestMethod.GET)
	public @ResponseBody User findUser(@RequestBody Long id){
		User user = new User();
		user.setUsername("jao");
		user.setPassword("1234");
		return user;
	}
	
}
