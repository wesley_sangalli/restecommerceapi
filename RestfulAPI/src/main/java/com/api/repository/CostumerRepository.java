package com.api.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.api.model.Customer;
import com.api.util.JPAUtil;

public class CostumerRepository {
	public List<Customer> findAll() {
		EntityManager manager = JPAUtil.getEntityManager();
		Query findAll = manager.createQuery("FROM Customer");
		@SuppressWarnings("unchecked")
		List<Customer> lista = findAll.getResultList();
		manager.close();
		return lista;
	}
}
