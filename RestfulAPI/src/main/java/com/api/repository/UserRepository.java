package com.api.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.api.model.User;
import com.api.util.JPAUtil;

public class UserRepository {
	
	public void createUser(User user){
		EntityManager manager = JPAUtil.getEntityManager();
		EntityTransaction ts = manager.getTransaction();
		ts.begin();
		if(user != null) manager.persist(user);
		ts.commit();
		manager.close();
	}
	
	public User findUserById(Long id){
		EntityManager manager = JPAUtil.getEntityManager();
		Query findById = manager.createQuery("FROM User Where id");
		User user = (User) findById.getSingleResult();
		manager.close();
		return user;
	}
	
	public List<User> findAll(){
		EntityManager manager = JPAUtil.getEntityManager();
		Query findAll = manager.createQuery("FROM User");
		@SuppressWarnings("unchecked")
		List<User> lista = findAll.getResultList();
		manager.close();
		return lista;
	}
}
