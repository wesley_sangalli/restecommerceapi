package com.api.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

public class BinaryUtil {
	
	
	public static String createMD5(String message) throws NoSuchAlgorithmException{
		MessageDigest digest = MessageDigest.getInstance("MD5");
		byte[] passwordDigested = digest.digest(message.getBytes());
		return toHexString(passwordDigested);
	}
	
	public static String toHexString(byte[] array) {
	    return DatatypeConverter.printHexBinary(array);
	}
}
