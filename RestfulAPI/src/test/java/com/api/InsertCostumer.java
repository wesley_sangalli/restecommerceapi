package com.api;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.junit.Test;

import com.api.model.Customer;
import com.api.util.JPAUtil;

public class InsertCostumer {

	@Test
	public void Insert(){
		EntityManager manager = JPAUtil.getEntityManager();
		EntityTransaction ts = manager.getTransaction();
		
		ts.begin();
		
		Customer costumer = new Customer();
		costumer.setName("Giorge Prado");
		costumer.setAdress("R. Olodum, 45 - São Paulo, SP");
		costumer.setPhone("(19) 99665477");
		
		manager.persist(costumer);
		
		ts.commit();
		manager.close();
	}
	
	@Test
	public void Find(){
		EntityManager manager = JPAUtil.getEntityManager();
		Query findAll = manager.createQuery("FROM Customer");
		@SuppressWarnings("unchecked")
		List<Customer> list = findAll.getResultList();
		
		for (Customer customer : list) {
			System.out.println(customer.getName());
		}
	}
}
